/*
    |--------------------------------------------------------------------------
    | JQuery 
    |--------------------------------------------------------------------------
    |
    | base_url creada como variable universal, declarada en views/footer. 
    |
    | //Check off-screen: función para verificar si un elemento de la página se encuentra fuera de la pantalla, devuelve true si
    |   algún valor (izquierda, derecha, arriba o abajo) devuelve 1px y false si todos los valores son 0
    |
    | //Preloader: ubicado dentro de /resources/views/slider, animación previa a la carga del slider. 
    | 
    | //Slider: Adaptación de la altura del slider a la altura de la ventana.
    |
    | //Destacados: Control del movimiento del carrusel de los destacados.
    |
    | //Servicios: Efecto parallax (fotografía fija de fondo cuando se hace scroll).
    |
    | //Login de usuario:
    |
    |   **Validaciones: Mediante la librería jquery.validation sobre /views/modal_forms_login
    |
    |   **Envío: Se lanza si el formulario está validado, se espera una respuesta del controlador /auth/LoginController.
    |
    |   **Reseteo: Elimina los estilos y mensajes de error al escribir de nuevo sobre el input.    
    |   
    | //Formulario anuncio de parcela:
    |
    |   **Botones de avance y retroceso: Cada fieldset de views/publish_orchard contiene un botón para avanzar y retroceder, aquí asignamos
    |     el valor current al fieldset del botón activo y lo usamos como referencia para avanzar o retroceder al fieldset que lo precede o le
    |     sigue. A continuación se envía el valor de aumento o disminución a la función de la barra de progreso. La acción para avanzar se 
    |     construye como una función que es llamada por cada botón después de hacer las **Validaciones 
    |
    |   **Barra de progreso: Formula que mediante el total de pasos que tiene el formulario recogidos en la variable steps aumenta o 
    |     disminuye el tamaño de la barra de progreso dependiendo del parametro que le llegue a través de los botones de avance y retroceso.
    |
    |   **Mensajes de ayuda para los inputs de presentación: Mediante la librería jquery.flip.min.js se utiliza primero función flip() sobre
    |     el contenedor que albergará los divs, flip(true) muestra la parte anterior del div flip(false) la posterior. Para activar el efecto
    |     en /views/publish_orchard asignamos la clase front y back a los div que queremos usar. 
    |    
    |   **Manipulación de botones de servicios y mensajes de ayuda : Los mensajes de ayuda utilizan la misma librería que "Mensajes de ayuda
    |     para los inputs de presentación". Se añade además cambio de estilo al pulsar encima de los botones y cambio de su valor (y/n). 
    |
    |   **Carga previa de foto: Si file-input cambia entonces la fotografía se carga en /views/modal-forms-photo mediante la url que genera
    |     el objeto FileReader. A continuación, los botones de cancel-button y upload-button descartan o aceptan la foto, si la foto es 
    |     aceptada de nuevo se recoge la url que generó FileReader y se genera una vista previa en miniatura. Queda pendiente afinar esta 
    |     sección para mejorar la interacción con las fotografías, tanto para marcar como foto destacada como borrarlas [01-06-2017] 
    |
    |   **Validaciones: Realizadas con https://jqueryvalidation.org/ , también se realizan validaciones del lado del servidor en 
    |     /app/Http/Requests/AddOrchardRequest. En errorPlacement se define como deben aparecer los mensajes de error, se manipula 
    |     el margin-bottom de .row para que cuando aparezcan los mensajes de error el formulario se adapte.
    |
    |     A continuación la función para enseñar los mensajes de error desde el servidor
    |     y no desde JQUERY Validator. Para ello se usa la salida error de ajax. Faltaría darle estilo.  #22-04-2017 
    |
    |     error: function(error) {
    |         var errors = JSON.parse(error.responseText);
    |
    |         $.each(errors, function(item,key) {
    |             $('#error-'+ item).text(key);
    |         })
    |     }        
    |
    |   **Envío de anuncio: e.preventDefault previene que no ocurra el comportamiento normal del botón submit, formData 
    |     crea un objeto con todos los datos recogidos en el formulario, aquellos que no son recogidos automáticamente al no ser inputs
    |     (como en el caso de los botones construidos a partir de una imagen) se pueden añadir mediante append. Se recoge todas las fotos
    |     que tienen miniatura recogiendo las urls generadas por FileReader (ver comentarios en "Carga previa de foto") y el valor de los
    |     botones de servicios. A continuación el método para comprobar los valores introducidos en formData a través de la consola:
    |    
    |     for(var pair of formData.entries()) {
    |         console.log(pair[0]+ ', '+ pair[1]);
    |      }
    |
    |     Al pasar a producción revisar que los enlaces /huertos/public/add_orchard que se utiliza en ajax funciona. Se hace desde
    |     ahí una llamada al controlador OrchardsController@addOrchard desde la ruta y se espera una respuesta. Si se crea el anuncio 
    |     se responde con la ID de la nueva parcela creada y se hace una redirección a su ficha /huertos/public/parcela mediante 
    |     OrchardsController@displayOrchard.
    |
    | //Buscador: 
    |
    |   **Autocompletado: Hace uso de la librería jquery.easy-autocomplete, y recoge los datos de App/Location@AutocompleteLocations.
    |     El evento onClickEvent lanza la búsqueda al seleccionar una opción de las desplegadas. En template se recogen los datos del
    |     servidor y se confeccionan el desplegable con la localización y el número total de parcelas anunciadas. getValue indica el
    |     nombre de la columna de la tabla de la base de datos que usaremos en el desplegable como opción principal. 
    |
    |   **Búsqueda de parcelas: Botón de buscar en la caja de búsqueda de inicio y en los resultados. Si no se ha introducido ninguna
    |     localización en la caja se devuelve por defecto todas las parcelas de Mallorca a través de App/SearchesController. Falta 
    |     trabajar la geolocalización [14-05-2017]
    |
    |   **Desplegar y cerrar menú de filtros: Modificaciones en las dimensiones de las columnas, creación de un div con etiqueta 
    |     "overlay" que desactiva todos los elementos de la pantalla menos el menú de filtros. Se crea una función closeFilters que
    |     deshace todos los cambios.  
    |  
    |   **Filtro de precios y superficie: Variación de las cifras que acompañan a los sliders cuando se desplazan éstos.
    |
    |   **Filtrar resultados: Se hace un split a los valores recogidos de los sliders de precio y area porque llegan el valor mínimo
    |     y máximo juntos separados por una coma. Se añaden a formData por separado mediante append. Sé utiliza window.location.href
    |     para distinguir si estamos filtrando los resultados de una provincia (por ejemplo /mallorca) o sólo los de una localidad 
    |     de una provincia (/mallorca/buger). Ambas posibilidades se distinguen con una ruta particular en routes/web
    |
    |   **Cambio de aspecto de search-header: Haciendo uso de la función isOnScreen se determina si el menú de cabecera de la web 
    |     está fuera de la pantalla y si es así se sustituye por la cabecera de los resultados de búsqueda. 
    |
    | //Ficha de anuncio: 
    |
    |   **Formulario de contacto desplegable: Hace visible elementos del formulario de contacto al pulsar sobre el campo de texto      
    |    
    */

//Check off-screen

$.fn.isOnScreen = function(test){

    var height = this.outerHeight();
    var width = this.outerWidth();

    if(!width || !height){
        return false;
    }

    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };

    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();

    bounds.right = bounds.left + width;
    bounds.bottom = bounds.top + height;

    var deltas = {
        top : viewport.bottom - bounds.top,
        left: viewport.right - bounds.left,
        bottom: bounds.bottom - viewport.top,
        right: bounds.right - viewport.left
    };

    if(typeof test == 'function') {
        return test.call(this, deltas);
    }

    return deltas.top > 0
    && deltas.left > 0
    && deltas.right > 0
    && deltas.bottom > 0;
};

//Preloader

$(window).load(function(){'use strict';
	$('.preloader').delay(1600).fadeOut('slow').remove();
});

//Slider

$(window).load(function(){'use strict';

	var slideHeight = $(window).height();
	$('#home .carousel-inner .item, #home .video-container').css('height',slideHeight);

	$(window).resize(function(){'use strict',
		$('#home .carousel-inner .item, #home .video-container').css('height',slideHeight);
	});
});

//Destacados

$(document).ready(function() {

  $('#media').carousel({
    pause: true,
    interval: false,
  });
});

//Servicios

$(function($) {'use strict',

    $(window).load(function(){'use strict',
        $('#services').parallax("50%", 0.3);
    });
});

//Registro de usuario

$("body").on("click", "#register-button", function(){

    /* Validaciones */

    $("#register-form").validate({

        onkeyup: false,
        onfocusout: false,

        rules: {
            name: {
                required: true,
                maxlength: 100
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 15
            }
        },

        messages: {
            name: {
                required: "*Escribe tu nombre",
                maxlength: "*Tu nombre es demasiado largo"
            },
            email: {
                required: "*Añade tu correo electrónico",
                email: "*Introduce un correo electrónico con un formato válido"
            },
            password: {
                required: "*Añade una contraseña", 
                minlength: "*La contraseña debe tener al menos 5 caracteres",
                maxlength: "*La contraseña debe tener menos de 15 caracteres"
            }
        },

        errorPlacement: function( error, element ) {
            error.insertAfter(element);
        }
    });

    /* Envío */

    if ($('#register-form').valid()){

        var formData = new FormData($('#register-form')[0]);

        $.ajax({
            type: 'post',
            url: base_url + '/register',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {              
                window.location = base_url + '/anunciar-parcela';
            },

            error: function(error){
                $('.authenticate-error-register').css('display','block');
            }
        });

        return false;
    }
});

    /* Reseteo de errores*/

    $("#register-form .form-control").keyup(function() {
        $(this).parent().find("input.error").removeClass("error");
        $(this).parent().find("label.error").remove();
        $('.authenticate-error-register').css('display','none');
    });

//Login de usuario

$("body").on("click", "#login-button", function(){

    /* Validaciones */

    $("#login-form").validate({

        onkeyup: false,
        onfocusout: false,

        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            }
        },

        messages: {
            email: {
                required: "*Debes escribir un correo electrónico",
                email: "*Introduce un correo electrónico con un formato válido"
            },
            password: {
                required: "*Introduce tu contraseña"
            }
        },

        errorPlacement: function( error, element ) {
            error.insertAfter(element);
        }
    });

    /* Envío */

    if ($('#login-form').valid()){

        var formData = new FormData($('#login-form')[0]);

        $.ajax({
            type: 'post',
            url: base_url + '/login',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {              
                window.location = base_url + '/mis-parcelas';
            },

            error: function(error){
                $('.authenticate-error-login').css('display','block');
            }
        });

        return false;
    }
});

    /* Reseteo de errores */

    $("#login-form .form-control").keyup(function() {
        $(this).parent().find("input.error").removeClass("error");
        $(this).parent().find("label.error").remove();
        $('.authenticate-error-login').css('display','none');
    });

//Formulario anuncio de parcela

$(document).ready(function() {

    /* Botones de avance y retroceso */

    var current = 1;
  
    function nextButton(button){
        var current_step = $(button).parent();
        var next_step = $(button).parent().next();
        next_step.show();
        current_step.hide();
        setProgressBar(++current);
    };

    $(".back-button").click(function(){
        var current_step = $(this).parent();
        var previous_step = $(this).parent().prev();
        previous_step.show();
        current_step.hide();
        setProgressBar(--current);
    });

    setProgressBar(current);

    /* Barra de progreso */

    function setProgressBar(curStep){
        var steps = $("fieldset").length;
        var percent = parseFloat(100 / steps) * curStep;
        var percent = percent.toFixed();

        $(".progress-bar")
          .css("width",percent+"%")
          .html(percent+"%");   
    }

    /* Mensajes de ayuda para los inputs de presentación */

    $("#help-description").flip({
        trigger: 'manual',
        axis: 'x',
        autoSize: true,
        reverse: true
    });

    $( ".description-options" ).focus(function() {
        var id = $(this).attr('name');
        $('#' + id + '-message').css('display', 'inline');
        $("#help-description").flip(true);
    });

    $( ".description-options" ).focusout(function() {
        var id = $(this).attr('name');
        $('#' + id + '-message').css('display', 'none');
        $("#help-description").flip(false);
    });

    /* Manipulación de botones de servicios y mensajes de ayuda */

    $('#help-services').flip({
        trigger: 'manual',
        axis: 'x',
        autoSize: true,
        reverse: true
    });

    $('body').on('click', '.services-button', function(){

        var value = $(this).val();
        var id = $(this).attr('id');
        var name = $(this).attr('name');

        if (value == 'n'){
            $(this).attr('id', id + '-active');
            $(this).val('y');
            $("#services-message").children().css('display','none');
            $('#' + name + '-message').css('display', 'inline');
            $("#help-services").flip(true);
        }
        else if (value == "y"){
            var id = $(this).attr('id').replace('-active','');
            $(this).attr('id', id);
            $(this).val('n');
        }
    });

    /* Carga previa de foto */

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#orchard-image').attr('src', e.target.result);               
                $('#photo').modal('show');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#file-input').change(function(){
        readURL(this);
    });

    $('body').on('click','#cancel-button', function(){
        $('#file-input').val('');
        $('#photo').modal('hide');
    });

    $('body').on('click','#upload-button', function(){
        var url = $('#orchard-image').attr('src');
        $('#photo').modal('hide');
        $( '.photos-container' ).prepend("<div class='col-lg-3 photo-orchard'><img id='photo-orchard' src='" 
            + url + "' /></div>");

        if ($('.photo-orchard').length == 8) {
            $('#upload-image').hide();
        };
    });

   $('body').on('click', '#photo-orchard', function() {
        $(this).parent().remove();
        $('#file-input').val('');

        if ($('.photo-orchard').length < 8) {
            $('#upload-image').show();
        };
    });

    /* Validaciones */

    var validate = $(".publish-orchard-form").validate({

        rules: {
            location: {
                required: true
            },
            ad_title: {
                required: true,
                minlength: 15,
                maxlength: 40
            },
            area: {
                required: true,
                range:[1,999999]
            },
            price: {
                required: true,
                range:[1,9999]
            },
            description: {
                required: true
            },
            photo_orchard: {
                required: true
            }
        },

        messages: {
            location: {
                required: "*Indica en qué municipio se encuentra tu parcela"
            },
            ad_title:{
                required: "*Añade un título a tu anuncio",
                minlength:"*Tu título es muy corto, añade algunas palabras",
                maxlength:"*Tu título es muy largo, quita algunas palabras"
            },
            area: {
                required: "*Introduce el tamaño de tu parcela",
                range: "*Sólo valores entre 1 y 999999"
            },
            price: {
                required: "*Introduce el precio de tu parcela",
                range: "*Sólo valores entre 1 y 9999"
            },
            description: {
                required: "*Añade una descripción de tu parcela"
            },
            photo_orchard: {
                required: "Añade al menos una foto a tu anuncio"
            }
        },

        errorPlacement: function( error, element ) {

            if (element.attr("name") == "location" | element.attr("name") == "photo_orchard") {
                $('.authenticate-error-publish').css('display','block').hide().fadeIn("fast");
                $(".error-message").html(error);
            } else {
                $('.error-form').css('display','block').hide().fadeIn("fast");
                error.appendTo("#error-content-description");
            }
        }
    });


    $('body').on('click', '.first-step', function() {
        
        if (validate.element("#publish-locations")){

            var formData = new FormData($('.publish-orchard-form')[0]);

            $.ajax({
                type: 'post',
                url: base_url + '/publish_orchard/location',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) { 
                    console.log(result);
                    $('.authenticate-error-publish').css('display','none');
                    $("input[name='locations_ID']").val(result.ID);
                    $("input[name='location_path']").val(result.path);
                    $("#price-message").html(Math.round(result.price));
                    $("#area-message").html(Math.round(result.area));
                    nextButton(".first-step");
                },
                error: function(error){
                    $('.authenticate-error-publish').css('display','block').hide().fadeIn("fast");
                    $(".error-message").html("Municipio no encontrado, elige uno de los listados");
                }
            });
        }
    });

    $('body').on('click', '.second-step', function() {
        
        var title = validate.element("#ad_title");
        var area = validate.element("#area");
        var price = validate.element("#price");
        var description = validate.element("#orchard-description");

        if (title & area & price & description){

            $('.authenticate-error-publish').css('display','none');
            nextButton(this);
        } else {
            $('.error-form').css('display','block').hide().fadeIn("fast");
        }
    });

    $('body').on('click', '.third-step', function() {

            nextButton(this);
    });


    $("body").on("click", ".finish-button", function(){

        if (validate.element("#file-input")){  

    /* Envío de anuncio */

            var formData = new FormData($('.publish-orchard-form')[0]);

            var photoNumber = 0;

            $(".photo-orchard").each(function(){
                formData.append("photo_" + photoNumber, $(this).children('img').attr("src"));
                photoNumber++;
            });

            formData.append("photo_number", photoNumber);

            $(".services-button").each(function(){
                formData.append($(this).attr('name'), $(this).val());
            });


            $.ajax({
                type: 'post',
                url: base_url + '/publish_orchard',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    
                    window.location = base_url + "/parcela/" + result;
                }
            });

            return false;
        }
    });

    /* Reseteo de errores */

    $(".publish-orchard-form").keyup(function() {
        if ($('#error-content-description').text() == ''){
            $('.error-form').fadeOut("fast").css('display','none');
        }
    });

//Buscador

$(document).ready(function() {

    /* Autocompletado */

    var options = {

        url: function(search) {
            return base_url + '/search/autocomplete/' + search;
        },

        list: {

            maxNumberOfElements: 6,

            match: {
                enabled: false
            },

            sort: {
                enabled: true
            },

            onClickEvent:  function() {

                var formData = new FormData($('.search-form')[0]);

                $.ajax({
                    type: 'post',
                    url: base_url + '/search/location',
                    dataType: 'json',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) {

                        window.location = base_url + "/alquiler-parcelas/" + result.province + "/" + result.location;
                    }
                });
            }   

        },

        template: {

            type: "custom",
            method: function(value, item) {

                if (item.total == "0"){
                    return  value +  "<span>(No hay parcelas anunciadas)</span>" ;
                }

                else if (item.total == "1"){
                    return  value + "<span>(Hay " + item.total + " parcela anunciada)</span>";
                }

                else {
                    return  value + "<span>(Hay " + item.total + " parcelas anunciadas)</span>";    
                }
            }
        },

        adjustWidth: false,
        placeholder: "En cualquier lugar...",
        getValue: "local"
    };

    $("#locations").easyAutocomplete(options);

    /* Búsqueda de parcelas */

    $(".search-form").on("submit", function(e){

        e.preventDefault();

        var formData = new FormData($('.search-form')[0]);

         for(var pair of formData.entries()) {
           console.log(pair[0]+ ', '+ pair[1]);
        }

        $.ajax({
            type: 'post',
            url: base_url + '/search/location',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {

                console.log(result);

                switch (result.location) {

                    case "":

                        window.location = base_url + "/alquiler-parcelas/" + result.province;

                        break

                    default:

                        window.location = base_url + "/alquiler-parcelas/" + result.province + "/" + result.location;

                        break;
                }
            }
        });

        return false;
    });

    /* Desplegar menu de filtros */

    $("#filter-options-button").click(function(e){

        e.preventDefault();

        $(".search-filter").css("display", "initial");
        $(".search-filter").hide().fadeIn("slow");
        $(".search-results").removeClass("col-lg-12").addClass( "col-lg-9").css("opacity","0.3");
        $(".results-container").removeClass("col-lg-3").addClass( "col-lg-4" );
        $("#search-results-title").removeClass("col-lg-6").addClass( "col-lg-8" );
        $("#filter-options").css("display", "none");
        $('<div></div>').prependTo('body').attr('id', 'overlay');
    });

    /* Cerra menu de filtros */

    var closeFilters = function(){
        $(".search-filter").css("display", "none");
        $(".search-results").removeClass("col-lg-9").addClass( "col-lg-12").css("opacity","1");
        $(".results-container").removeClass("col-lg-4").addClass( "col-lg-3" );
        $("#search-results-title").removeClass("col-lg-8").addClass( "col-lg-6" );
        $("#filter-options").css("display", "initial");
        $("#overlay").removeAttr("id","overlay");
    };

    $("#close-filters").click(function(e){

        e.preventDefault();

        closeFilters();
    });

    /* Filtro de Precios */

    $("#price-filter-slider").slider({
        tooltip: 'hide'
    });

    $("#price-filter-slider").on("slide", function(slideEvt) {
        $("#min-price").text(slideEvt.value[0] + " €");
        $("#max-price").text(slideEvt.value[1] + " €");
    });

    /* Filtro de Superficie */

    $("#area-filter-slider").slider({
        tooltip: 'hide'
    });

    $("#area-filter-slider").on("slide", function(slideEvt) {
        $("#min-area").text(slideEvt.value[0] + " m²");
        $("#max-area").text(slideEvt.value[1] + " m²");
    });

    /* Filtrar resultados */

    $("#filter-form").on("submit", function(e){

        e.preventDefault();

        var priceRange = $('#price-filter-slider').val().split(",");
        var areaRange = $('#area-filter-slider').val().split(",");

        min_price = priceRange[0];
        max_price = priceRange[1];
        min_area = areaRange[0];
        max_area = areaRange[1];

        var formData = new FormData($('#filter-form')[0]);

        formData.append("min_price", min_price);
        formData.append("max_price", max_price);
        formData.append("min_area", min_area);
        formData.append("max_area", max_area);

        $.ajax({
            type: 'post',
            url: window.location.href + '/filters',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                $("#search-results-container").html(response);
                closeFilters();
            }
        });

        return false;
    });

    /*Cambio de aspecto de search-header */

    $(window).on('scroll', function (e) {

        if($(".navbar-collapse").isOnScreen()){
            $("#search-header-offset").removeAttr("id","search-header-offset").attr("id","search-header");
        }else{
            $("#search-header").removeAttr("id","search-header").attr("id","search-header-offset");
        }
    });
});

    /*Autocompletado localizaciones*/

    $(document).ready(function() {

        var publishOptions = {

            url: function(search) {
                var province = $('select[name=province_ID] option:selected').val();
                return base_url + '/search/autocomplete/' + search + '/' + province;
            },

            list: {

                maxNumberOfElements: 3,

                match: {
                    enabled: false
                },

                sort: {
                    enabled: true
                }
            },

            template: {

                type: "custom",
                method: function(value, item) {
         
                        return  value;
                }
            },

            adjustWidth: false,
            placeholder: "Busca su nombre...",
            getValue: "local"
        };

        $("#publish-locations").easyAutocomplete(publishOptions);

    });

//Ficha de anuncio

$(document).ready(function() {

    /* Formulario de contacto desplegable */

    $('#user-data-container').hide();

    $('#message-field').focus(function() {
       $('#user-data-container').show();
    });


    });

});




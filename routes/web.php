<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|	http://stackoverflow.com/questions/34727005/laravel-pass-value-from-controller-to-route-and-then-to-controller
|	Los parametros pasados a la Home provienen de /App/Providers/ViewComposerServiceProvider
|
|	**'alquiler-parcelas': ahora mismo apunta a mallorca, se debería hacer más flexible más adelante. [14-05-2017]
|
*/

Route::get('/', function()
{
	return view('Home', compact('featured'));
});

Route::post('publish_orchard', 'OrchardsController@publishOrchard');

Route::post('publish_orchard/location', 'SearchesController@getLocation');

Route::get('anunciar-parcela', function(){
	return view('publish_orchard');
})->middleware('auth')->name('publish-orchard');

Route::get('mis-parcelas', function(){
	return view('my_orchards');
})->middleware('auth')->name('my-orchards');

Route::get('search/autocomplete/{location}', 'SearchesController@autocompleteLocations');

Route::get('search/autocomplete/{location}/{province}', 'SearchesController@autocompleteLocations');

Route::post('search/location', 'SearchesController@getPath');

Route::get('alquiler-parcelas', function () {
    return redirect('alquiler-parcelas/mallorca');
});

Route::get('alquiler-parcelas/{province}', 'OrchardsController@displayOrchards');

Route::post('alquiler-parcelas/{province}/filters', 'SearchesController@filters');

Route::get('alquiler-parcelas/{province}/{location}', 'OrchardsController@displayOrchards');

Route::post('alquiler-parcelas/{province}/{location}/filters', 'SearchesController@filters');

Route::get('parcela/{id}', 'OrchardsController@displayOrchard')->where('id', '[0-9]+')->name('single');

Auth::routes();


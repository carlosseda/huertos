<!--
    |
    | Pie de página
    |
    |  Se carga en el pie de página todo el javascript para no bloquear la carga de la página. 
    |
    |   *Variable superglobal base_url que se declara para tener url relativas en las mismas llamadas ajax desde distintas páginas
    |
    |   *bootstrap-slider: slider de precios y superficie en los filtros de búsqueda. 
    |
    |	*Jquery.easy-autocomplete: se encarga del autocompletado del buscador
	|
    |	*Smoomthscroll y parallax: hacen el efecto de fotografía fija con el scroll en la página de inicio. 
    |
    |   *Flip: Añade transición 180 grados a div's en los help-messages en la creación de un anuncio
    |
    |	*Validate: Valida la información introducida en los formularios
    |    
    -->

<footer id="footer">
	<div class="container">
		<div class="text-center">
			<p>@lang('footer.copyright')</p>
		</div>
	</div>
    <script type="text/javascript">var base_url = {!! json_encode(url('/')) !!};</script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-slider.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.easy-autocomplete.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/smoothscroll.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.parallax.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.flip.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
	<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
    <![endif]-->

</footer> 
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="photo">
	<div class="modal-dialog modal-dialog-photo">
		<div class="modal-content">
			<div class="modal-body">
				<div class="container">
				    <div class="row">
				    	<div class="col-lg-12">
				    		<img id="orchard-image" src="">
				    	</div>
				    </div>

				    <div class="row">
				    	<div class="col-lg-10">
				    		<h4>¿Quieres que esta sea la foto principal de tu parcela?</h4>
				    	</div>
				    	<div class="col-lg-2">
				    		<input type="checkbox" name="remember">
				    	</div>
				    </div>

				    <div class="row">
				    	<div class="col-lg-6 submit-photo">
				    		<button type="submit" id="cancel-button" class="btn btn-primary">
				                Cancelar
		                    </button>
				    	</div>
				    	<div class="col-lg-6 submit-photo">
				    		<button type="submit" id="upload-button" class="btn btn-primary">
				                Subir foto
		                    </button>
				    	</div>
				    </div>

				</div>
			</div>
		</div>
	</div>
</div>
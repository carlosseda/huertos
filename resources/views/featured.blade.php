<!--
    |
    | Carrusel de parcelas destacadas
    |
    |   $featured es recogido de la base de datos con el modelo app/Http/Orchard usando app/Providers/ViewComposerServiceProvider.
    |   el cual hace que $featured esté disponible en cualquier vista. 
    |
    |   foreach->chunk limita el número de elementos tratados en la colección de datos recibidos a través de $featured, se eligen 3
    |   ya que será el número de parcelas mostradas en el carrusel en cada fila. 
    |
    |   $loop->first es un método propio de laravel, si nos encontramos en la primera iteración del foreach realiza una acción 
    |   concreta, en este caso el nombre de la clase será "item active" que indica a bootstrap que este div es el primero del 
    |   carrusel.  
    |    
    -->

<section id="featured">
    <div class="container">
        <div class="row clearfix">
            <div class="col-sm-8 col-sm-offset-1">
                <h2 id="featured-title">@lang('featured.title')</h2>
            </div>
        </div>
        <div class="row">
            <div class='col-md-12'>
                <div class="carousel slide media-carousel" id="media">
                    <div class="carousel-inner">

                        @foreach($featured->chunk(3) as $item)   
                            <div class="item {{ $loop->first ? ' active' : '' }}">

                                @foreach($item as $featured => $value)
                                    <div class="col-md-4">
                                        <div class="single-featured">
                                            <div id="featured-image">
                                                <a href="{{URL::asset('/parcela/' . $value->id)}}"><img src="{{ URL::asset($value->directory . $value->photo_title . $value->photo_path . $value->location_path . '-' . $value->size . '.' . $value->extension) }}" alt="" /></a>
                                            </div>
                                            <div id="featured-details">
                                                <a href="{{URL::asset('/parcela/' . $value->id)}}"><h2>@lang('featured.orchard-title') {{$value->local}}@lang('featured.locate')</h2></a>
                                                <ul class="post-meta">
                                                    <li><strong>@lang('featured.area')</strong>{{$value->area}} @lang('featured.area_suffix')</li>
                                                    <li><strong>@lang('featured.price')</strong>{{$value->price}} @lang('featured.price_suffix')</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>       
                                @endforeach 
                            
                            </div>
                        @endforeach
                        
                    </div>
                    <a data-slide="prev" href="#media" class="left carousel-control hidden-xs"><i class="fa fa-angle-left fa-4x"></i></a>
                    <a data-slide="next" href="#media" class="right carousel-control hidden-xs"><i class="fa fa-angle-right fa-4x"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
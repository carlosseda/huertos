<!--
    |
    | Ficha individual de parcela
    |
    | $orchard es recogida a través de OrchardsController@displayOrchard
    |    
    -->

@extends('layouts.master')

@section("content")

<section class="single-orchard">
	<div class="container">
		<div class="single-orchard-container">

			<div class="row">
				<div class="col-lg-12" id="single-orchard-photo">	          
			        <img src="{{ URL::asset($orchard->directory . $orchard->photo_title . $orchard->photo_path . $orchard->location_path . '-' . $orchard->size . '.' . $orchard->extension) }}">            
			    </div>
		    </div>

		    <div class="row">
			    <div class="col-lg-9" id="single-orchard-details">
			    	<h2>@lang('single_orchard.title') {{$orchard->local}}@lang('single_orchard.locate') </h2>

			    	<div id="single-orchard-services">
				    	<h3>@lang('single_orchard.services')</h3>

				    	<div class="row single-orchard-services-container">
				    		<div class="col-lg-3 option-orchard-services">
				    			<h4 id="{{$orchard->parking}}">@lang('single_orchard.parking_label')</h4>
				    		</div>
				    		<div class="col-lg-3 option-orchard-services">
				    			<h4 id="{{$orchard->electricity}}">@lang('single_orchard.electricity_label')</h4>
				    		</div>
				    		<div class="col-lg-3 option-orchard-services">
				    			<h4 id="{{$orchard->warehouse}}">@lang('single_orchard.warehouse_label')</h4>
				    		</div>
				    		<div class="col-lg-3 option-orchard-services">
				    			<h4 id="{{$orchard->tools}}">@lang('single_orchard.tools_label')</h4>
				    		</div>
				    	</div>
				    	<div class="row single-orchard-services-container">
				    		<div class="col-lg-3 option-orchard-services">
				    			<h4 id="{{$orchard->well}}">@lang('single_orchard.well_label')</h4>
				    		</div>
				    		<div class="col-lg-3 option-orchard-services">
				    			<h4 id="{{$orchard->fenced}}">@lang('single_orchard.fenced_label')</h4>
				    		</div>
				    		<div class="col-lg-3 option-orchard-services">
				    			<h4 id="{{$orchard->farmyard}}">@lang('single_orchard.farmyard_label')</h4>
				    		</div>
				    		<div class="col-lg-3 option-orchard-services">
				    			<h4 id='{{$orchard->greenhouse}}'>@lang('single_orchard.greenhouse_label')</h4>
				    		</div>
				    	</div>

			    	</div>

			    	<div class="row">
			    		<div class="col-lg-4">	          
					        <h4>@lang('single_orchard.description_title')</h4>
					    </div>
					    <div class="col-lg-8">	          
				    		<p>{{$orchard->description}}</p>  
					    </div>
			    	</div>

		    	</div>

	    		<div class="col-lg-3" id="contact-container">

		    		<div class ="row" id="contact-header">
		        		<div class="col-lg-5">
		        		 	<p id="price">{{$orchard->price}} <span>@lang('single_orchard.price')</span></p>
		        		</div>
		        		<div class="col-lg-5">
		        		 	<p id="area">{{$orchard->area}} <span>@lang('single_orchard.area')</span></p>
		        		</div>
		        	</div>

               		<div class="col-lg-12 col-xl-12" id ="contact-form">
              			
              			{{Form::open()}}

	                	<h3>@lang('single_orchard.form_title')</h3>

	            	 	{{Form::textarea('message', null, ['id' => 'message-field','class' => 'form-control',
	                    'rows' => 5, 'columns' => 15, 'placeholder' => Lang::get('single_orchard.message_field') ]) }}

	                    <div id="user-data-container">
		                    {{Form::label('email', Lang::get('single_orchard.email_label'))}}
		                    {{Form::email('email',null, ['class' => 'contact'])}}

		                    <div class ="row">
				        		<div class="col-lg-12">
				        		 	 {{Form::label('name', Lang::get('single_orchard.name_label'))}}
			               			 {{ Form::text('name', '', array('class' => 'contact')) }}
				        		</div>
				        	</div>	
			        	</div>
	            
	                	{{Form::submit(Lang::get('single_orchard.submit_button'), array('class' => 'btn btn-lg','id' => 'send-message'))}}

	                    <p id="disclaimer">@lang('single_orchard.disclaimer_message')</p>
	                  
	                   	{{Form::close()}}

	                </div>
	    		</div>

	    	</div>
     	</div>
	</div>
</section>

@endsection


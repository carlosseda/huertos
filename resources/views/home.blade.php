<!--
    |
    | Home
    |
    |  Extiende la plantilla padre /views/layouts/master
    |   
    |    
    -->

@extends('layouts.master')

@section("content")

    @include("slider")
    @include("search_form")
    @include("featured")
    @include("services")

@endsection

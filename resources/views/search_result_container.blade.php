<!--
    |
    | Resultados de búsqueda de parcelas
    |
    | $orchards es recogida a través de OrchardsController@displayOrchars o OrchardsController@displayOrcharsLocation
    |    
    -->

@foreach($orchards as $orchards => $value)
    <div class="col-lg-3 results-container">
        <div class="single-orchard">
            <div id="orchard-image">
                <a href="{{URL::asset('/parcela/' . $value->id)}}"><img src="{{ URL::asset($value->directory . $value->photo_title . $value->photo_path . $value->location_path . '-' . $value->size . '.' . $value->extension) }}" alt="" /></a>
            </div>
            <div id="orchard-details">
                @empty($local)
                    <a href="{{URL::asset('/parcela/' . $value->id)}}"><h3>@lang('search_result.orchard-title') {{$value->local}}</h3></a>
                @endempty
                <ul class="post-meta">
                    <li><strong>@lang('search_result.area')</strong>{{$value->area}}@lang('search_result.area_suffix')</li>
                    <li><strong>@lang('search_result.price')</strong>{{$value->price}} @lang('search_result.price_suffix')</li>
                </ul>
            </div>
        </div>
    </div>       
@endforeach   

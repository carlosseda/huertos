<!--
    |
    | Formulario para filtrar resultados
    |
    |   El precio y el tamaño mínimo y máximo son cogidos de la base de datos y se adaptan a los resultados que se muestran. Se |   
    |   utiliza la librería bootstrap-slider.js para los slider, configurada en el archivo main.js. 
    |   
    |    
    -->


<aside class="search-filter col-sm-3 col-lg-3">

  <div class="row filters-header">
    <div class="col-lg-10" id="search-filter-title">
          <h3>@lang('search_result.title_filters')</h3>
    </div>
    <div class="col-lg-2" id="close-search-filter">
          <button type="button" id="close-filters" class="close" data-dismiss="modal" aria-hidden="true"><img src="{{ URL::asset('/images/ico/close-filters.png') }}"></button>
    </div>
  </div>

  {{Form::open(array('id'=>'filter-form')) }}

    <div class="row">
        <div class="col-lg-12 filter-title">
          <label for="price-filter">{{Lang::get('search_result.price-filter-label')}}</label>
        </div>
    </div>

    <div class="row" id="price-range">
        <div class="col-lg-6 range-container">
            <span id="min-price">{{$min_price}} {{Lang::get('search_result.price-filter')}}</span> 
        </div>
        <div class="col-lg-6 range-container">
            <span id="max-price">{{$max_price}} {{Lang::get('search_result.price-filter')}}</span> 
        </div>
    </div>

    <div class="row" id="price-filter">
      <div class="col-lg-12">
        <input id="price-filter-slider" name="price" type="text" class="span2" value="" data-slider-min="{{$min_price}}" data-slider-max="{{$max_price}}" data-slider-step="1" data-slider-value="[{{$min_price}},{{$max_price}}]"/>
      </div>
    </div>

    <div class="row">
        <div class="col-lg-12 filter-title">
          <label for="area-filter">{{Lang::get('search_result.area-filter-label')}} </label>
        </div>
    </div>

    <div class="row" id="area-range">
        <div class="col-lg-6 range-container">
            <span id="min-area">{{$min_area}} {{Lang::get('search_result.area-filter')}}</span> 
        </div>
        <div class="col-lg-6 range-container">
            <span id="max-area">{{$max_area}} {{Lang::get('search_result.area-filter')}}</span> 
        </div>
    </div>

    <div class="row" id="area-filter">
      <div class="col-lg-12">
        <input id="area-filter-slider" type="text" name="area" class="span2" value="" data-slider-min="{{$min_area}}" data-slider-max="{{$max_area}}" data-slider-step="1" data-slider-value="[{{$min_area}},{{$max_area}}]"/>
      </div>
    </div>

    <div class="row">
        <div class="col-lg-12 filter-title">
          <label for="area-filter">{{Lang::get('search_result.services-filter-label')}} </label>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 service-container">
            {{ Form::checkbox('electricity', 'y') }}<span class="service-label">{{Lang::get('search_result.electricity-filter-label')}}</span> 
        </div>
        <div class="col-lg-6 service-container">
            {{ Form::checkbox('well', 'y') }}<span class="service-label">{{Lang::get('search_result.well-filter-label')}}</span> 
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 service-container">
            {{ Form::checkbox('parking', 'y') }}<span class="service-label">{{Lang::get('search_result.parking-filter-label')}}</span> 
        </div>
        <div class="col-lg-6 service-container">
            {{ Form::checkbox('fenced', 'y') }}<span class="service-label">{{Lang::get('search_result.fenced-filter-label')}}</span> 
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 service-container">
            {{ Form::checkbox('warehouse', 'y') }}<span class="service-label">{{Lang::get('search_result.warehouse-filter-label')}}</span> 
        </div>
        <div class="col-lg-6 service-container">
            {{ Form::checkbox('tools', 'y') }}<span class="service-label">{{Lang::get('search_result.tools-filter-label')}}</span> 
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 service-container">
            {{ Form::checkbox('farmyard', 'y') }}<span class="service-label">{{Lang::get('search_result.farmyard-filter-label')}}</span> 
        </div>
        <div class="col-lg-6 service-container">
            {{ Form::checkbox('greenhouse', 'y') }}<span class="service-label">{{Lang::get('search_result.greenhouse-filter-label')}}</span> 
        </div>
    </div>

  {{Form::submit(Lang::get('search_result.filter-results-button'), array('class' => 'btn btn-lg','id' => 'filter-results-button'))}}

  {{Form::close()}}

</aside>
<!--
    |
    | Formulario de búsqueda de la página de inicio
    |
    |  Hace uso de la siguiente librería: http://easyautocomplete.com y la llamada a la base de datos se hace a través de main.js
    |   
    |    
    -->

<section id="search">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="search-form-container col-sm-12 ">
					{{Form::open(array('class'=>'search-form'))}}
						<div id="search-location-container">
							<label>@lang('search_form.location_label')</label>
							<input id="locations" name="locations">
						</div>
						<div id="search-submit-container">
							{{Form::submit(Lang::get('search_form.submit_button'), array('id' => 'send-search'))}}	
						</div>
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
</section>

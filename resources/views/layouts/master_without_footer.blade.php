<!--
    |
    |    
    -->

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@lang('head.title')</title>
		<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/bootstrap-slider.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/responsive.css') }}" rel="stylesheet">
		<link rel="shortcut icon" href="{{ URL::asset('images/ico/favicon.png') }}">
		<script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
	</head><!--/head-->
	<body>
      	@include("header")
       	@yield("content")
       	<footer id="js">
		    <script type="text/javascript">var base_url = {!! json_encode(url('/')) !!};</script>
			<script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>
			<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
		    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-slider.js') }}"></script>
			<script type="text/javascript" src="{{ URL::asset('js/jquery.easy-autocomplete.min.js') }}"></script>
			<script type="text/javascript" src="{{ URL::asset('js/smoothscroll.js') }}"></script>
			<script type="text/javascript" src="{{ URL::asset('js/jquery.parallax.js') }}"></script>
			<script type="text/javascript" src="{{ URL::asset('js/jquery.flip.min.js') }}"></script>
			<script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
			<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
			<!--[if lt IE 9]>
		    <script src="js/html5shiv.js"></script>
			<script src="js/respond.min.js"></script>
		    <![endif]-->
		</footer> 
	</body>
</html>
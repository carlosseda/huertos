<!--
    |
    | Plantilla padre
    |
    |  Esta plantilla se usará de base para el resto de vistas. Los archivos js se enlazan en el footer para que no retrasen la
    |  carga de la página.
    |   
    |    
    -->

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@lang('head.title')</title>
		<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/bootstrap-slider.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
		<link href="{{ URL::asset('css/responsive.css') }}" rel="stylesheet">
		<link rel="shortcut icon" href="{{ URL::asset('images/ico/favicon.png') }}">
		<script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
	</head><!--/head-->
	<body>
        @include("header")
        @yield("content")
        @include("footer")
	</body>
</html>
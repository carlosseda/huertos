<section id="slider">
    <div class="preloader">
        <div class="preloder-wrap">
            <div class="preloder-inner">
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
            </div>
        </div>
    </div><!--/.preloader animación de inicio para visualizar al usuario que se está cargando un slider-->
    <div id="main-carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active" style="background-image: url(images/slider/slider3.svg)">
                <div class="carousel-caption">
                   <div class="row clearfix">
                        <div class="col-lg-4 col-sm-offset-7">
                            <h1 class="heading">¿Quieres cultivar en tu propio huerto? ¡Empieza a buscar tu parcela ideal en Mallorca!</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-left member-carousel-control hidden-xs" href="#main-carousel" data-slide="prev"><i class="fa fa-chevron-circle-left"></i></a>
         <a class="carousel-right member-carousel-control hidden-xs" href="#main-carousel" data-slide="next"><i class="fa fa-chevron-circle-right"></i></a>
    </div>
</section><!--/.carousel-inner Slider principal-->
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="register">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="container">
				    <div class="row">
				        <div class="col-md-12">

				       		<div class="row authenticate-error authenticate-error-register">
				        		<div class="col-lg-2 authenticate-error-ico">
									<img src="{{ URL::asset('images/ico/exclamation-ico.png') }}" alt="exclamation">
				        		</div>
				        		<div class="col-lg-10 authenticate-error-message">
				        			<p>Este correo ya existe.</p>
				        		</div>
				        	</div>
				        
		                    <form class="form-horizontal" id="register-form" role="form" method="POST" action="{{route('register')}}">
		                        {{ csrf_field() }}

		                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		                            <div class="col-md-12">
		                                <input id="name" type="text" class="form-control" name="name" placeholder="Nombre" value="{{ old('name') }}" required autofocus>

		                                @if ($errors->has('name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('name') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		                            <div class="col-md-12">
		                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo electrónico" required>

		                                @if ($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		                            <div class="col-md-12">
		                                <input id="password" type="password" class="form-control" name="password" 
		                                placeholder="Elige una contraseña" required>

		                                @if ($errors->has('password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <button type="submit" id="register-button" class="btn btn-primary">
		                                    ¿Empezamos?
		                                </button>
		                            </div>
		                        </div>

		                    </form>

	                    </div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
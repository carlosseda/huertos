<!--
    |
    | Cabecera/Plantilla de los resultados de búsqueda
    |
    |  search_result_filters se activa a través de main.js. 
    |   
    |    
    -->

@extends('layouts.master')

@section("content")

<div class="container">
    <div class="row">

        <section class="search-results col-lg-12">

            <div class="row" id="search-header">

                <div class="col-lg-6" id="search-results-title">
                    <h2>
                        @lang('search_result.title_results') 
                        @isset($local){{$local}}@lang('search_result.optional_locate') @endisset 
                        @empty($local) @lang('search_result.locate') @endempty
                    </h2>
                </div>

                <div class="col-lg-4" id="search-location">
                    {{Form::open(array('class'=>'search-form'))}}
                        <div id="search-location-container">
                            <input id="locations" name="locations">
                        </div>
                        <div id="search-submit-container">
                            {{Form::submit('', array('id' => 'send-search'))}}   
                        </div>
                    {{Form::close()}}
                </div>

                <div class="col-lg-2" id="filter-options">
                    {{Form::submit(Lang::get('search_result.filter-results-button'), array('class' => 'btn btn-lg','id' => 'filter-options-button'))}}
                </div>

            </div>

            <div class="row" id="search-results-container">
                @include("search_result_container")
            </div> 
            
        </section>

        @include("search_result_filters")

    </div>
</div>

@endsection

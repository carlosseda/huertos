 <!--
    |
    | Cabecera
    |
    |  Carga de las ventanas modal, logo, y opciones de menú de cabecera. 
    |   
    |    
    -->

<header id="navigation">
    @include("modal_forms_register")
    @include("modal_forms_login")
    <nav class="navbar" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::asset('') }}"><img src="{{ URL::asset('images/logo.png') }}" alt="logo"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li class="scroll"><a href="" data-toggle="modal" data-target="#register">@lang('header.add_orchard')</a></li>
                        <li class="scroll"><a href="" data-toggle="modal" data-target="#login">@lang('header.login')</a></li>
                        <li class="scroll"><a href="">@lang('header.help')</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('publish-orchard') }}">Anunciar Parcela</a>
                                    <a href="{{ route('my-orchards') }}">Mis parcelas</a>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;
                                    ">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>
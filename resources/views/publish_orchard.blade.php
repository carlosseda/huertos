<!--
    |
    | Publicación de anuncio 
    |    
    -->
@include("modal_forms_photo")

@extends('layouts.master_without_footer')

@section("content")

<div class="container">
    <div class="row">

        <section class="publish-orchard col-lg-12">

        <div class="row authenticate-error authenticate-error-publish">
            <div class="col-lg-1 col-lg-offset-4 authenticate-error-ico">
                <img src="{{ URL::asset('images/ico/exclamation-ico.png') }}" alt="exclamation">
            </div>
            <div class="col-lg-6 authenticate-error-message">
                <p class="error-message"></p>
            </div>
        </div>

        {{Form::open(array('files' => true, 'class'=>'publish-orchard-form'))}}

        <input type="hidden" id="token" value="{{ csrf_token() }}">

            <fieldset>
                <div class="row">

                    <div class="publish-form-container col-lg-6">

                        <div class="col-lg-9 col-lg-offset-2 form-title">
                            <h2>@lang('publish_orchard.location_title')</h2> 
                        </div>

                        <div class="col-lg-11 col-lg-offset-2 option-title">
                            {{Form::label('province_ID', Lang::get('publish_orchard.province_option'))}}
                        </div>

                        <div class="col-lg-11 col-lg-offset-2" id="province-select">
                            <select  name="province_ID" id="province">
                                @foreach ($provinces as $province => $value)
                                    <option id="{{$value->path}}" value="{{$value->ID}}">{{$value->province}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-11 col-lg-offset-2 option-title">
                            {{Form::label('location', Lang::get('publish_orchard.location_option'))}}
                        </div>

                        <div class=" col-lg-11 col-lg-offset-2">
                            <div id="publish-location-container">
                                <input id="publish-locations" name="location">
                                {{ Form::hidden('locations_ID') }}
                                {{ Form::hidden('location_path') }}
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 help">
                        <div class="image-form">
                            <div id="location-ico"> 
                            </div>
                        </div>
                    </div>

                </div>

                {{Form::button(Lang::get('publish_orchard.next_button'), array('class' => 'btn btn-lg next-button first-step','id' => 'next-button-center'))}}
                                    
            </fieldset>

            <fieldset>
                
                <div class="row">

                    <div class="publish-form-container col-lg-6">
                        <div class="col-lg-9 col-lg-offset-2 form-title">
                            <h2>@lang('publish_orchard.description_title')</h2> 
                        </div>

                        <div class="col-lg-11 col-lg-offset-2 option-title">
                            {{Form::label('ad_title', Lang::get('publish_orchard.ad_title'))}}
                        </div>

                        <div class="col-lg-11 col-lg-offset-2">
                            {{ Form::text('ad_title', '', array('id' => 'ad_title', 'class' => 'description-options')) }}
                        </div>

                        <div class="row price-area-container">
                            <div class="col-lg-4 col-lg-offset-2 option-title">
                                {{Form::label('area', Lang::get('publish_orchard.area_option'))}}
                            </div>

                            <div class="col-lg-4  option-title" id="area-title">
                                {{Form::label('price', Lang::get('publish_orchard.price_option'))}}
                            </div>

                            <div class="col-lg-4 col-lg-offset-2">
                                {{Form::input('number','area',null, ['min'=>1,'id'=>'area','class'=>'description-options'])}}
                            </div>

                            <div class="col-lg-4">
                                {{Form::input('number','price',null, ['min'=>1,'id'=>'price','class'=>'description-options'])}}
                            </div>
                        </div>

                        <div class="col-lg-11 col-lg-offset-2 option-title">
                            {{Form::label('description', Lang::get('publish_orchard.description_option'))}}
                        </div>

                        <div class="col-lg-11 col-lg-offset-2">
                            {{Form::textarea('description', null, ['id' => 'orchard-description','class' => 'form-control description-options',
                                'rows' => 7, 'columns' => 10 ]) }}
                        </div>

                    </div>

                    <div class="col-lg-6 help">
                        <div class="help-form" id="help-description">
                            <div class="row help-main-message front">
                                <div class="col-lg-9 help-content">
                                    <h4>Una presentación detallada de tu anuncio llamará la atención del resto de usuarios y les servirá para conocer todo lo que ofreces con la parcela.</h4>
                                </div>
                                <div class="col-lg-3 help-ico"> 
                                </div>
                            </div>
                            <div class="row help-message back">
                                <div class="col-lg-9 help-content"> 
                                    <h4 id="ad_title-message">El título de tu anuncio puede servir para destacar alguna cualidad destacada de tu parcela. Por ejemplo: "Parcela con pozo y electricidad", "Parcela preparada para tener animales", "Parcela abonada lista para plantar" </h4>
                                    <h4 id="price-message">El precio medio por metro cuadrado en tu zona es de 15 €</h4>
                                    <h4 id="area-message">El tamaño medio de las parcelas alquiladas en tu zona es de 20 m²</h4>
                                    <h4 id="description-message">Describe detalladamente las condiciones de uso de la parcela, los servicios que ofreces, etc</h4>
                                </div>
                                <div class="col-lg-3 help-ico"> 
                                </div>
                            </div>
                        </div>

                        <div class="error-form">
                            <div class="row error-main-message">
                                <div class="col-lg-9 error-content">
                                    <h4 id="error-content-description"></h4>
                                </div>
                                <div class="col-lg-3 error-ico"> 
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                {{Form::button(Lang::get('publish_orchard.back_button'), array('class' => 'btn btn-lg back-button','id' => 'back-button'))}}
           
                {{Form::button(Lang::get('publish_orchard.next_button'), array('class' => 'btn btn-lg next-button second-step','id' => 'next-button'))}}
                  
            </fieldset>

            <fieldset>
                
                <div class="row">

                    <div class="publish-form-container col-lg-6">
                        <div class="col-lg-9 col-lg-offset-2 form-title">
                            <h2>@lang('publish_orchard.services_title')</h2> 
                        </div>

                        <div class="row">
                            <div class="col-lg-5 col-lg-offset-2">
                                {{Form::button(null, ['class' => 'services-button',
                                'id' => 'fenced-button','value' => 'n', 'name' => 'fenced'])}} 
                            </div>

                            <div class="col-lg-5">
                                {{Form::button(null, ['class' => 'services-button',
                                'id' => 'parking-button','value' => 'n', 'name' => 'parking'])}}           
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-5 col-lg-offset-2">
                                {{Form::button(null, ['class' => 'services-button',
                                'id' => 'electricity-button','value' => 'n', 'name' => 'electricity'])}} 
                            </div>

                            <div class="col-lg-5">
                                {{Form::button(null, ['class' => 'services-button',
                                'id' => 'well-button','value' => 'n', 'name' => 'well'])}}                   
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-5 col-lg-offset-2">
                                {{Form::button(null, ['class' => 'services-button',
                                'id' => 'tools-button','value' => 'n', 'name' => 'tools'])}} 
                            </div>

                            <div class="col-lg-5">
                                {{Form::button(null, ['class' => 'services-button',
                                'id' => 'warehouse-button','value' => 'n', 'name' => 'warehouse'])}} 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-5 col-lg-offset-2">
                                {{Form::button(null, ['class' => 'services-button',
                                'id' => 'farmyard-button','value' => 'n', 'name' => 'farmyard'])}}
                            </div>

                            <div class="col-lg-5">
                                {{Form::button(null, ['class' => 'services-button',
                                'id' => 'greenhouse-button','value' => 'n', 'name' => 'greenhouse'])}}
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-6 help">
                        <div class="help-form" id="help-services">
                            <div class="row help-main-message front">
                                <div class="col-lg-9 help-content">
                                    <h4>Una presentación detallada llamará la atención del resto de usuarios y les servirá para conocer todo lo que ofreces con la parcela. </h4>
                                </div>
                                <div class="col-lg-3 help-ico"> 
                                </div>
                            </div>
                            <div class="row help-message back">
                                <div class="col-lg-9 help-content" id="services-message"> 
                                    <h4 id="fenced-message">Fenced</h4>
                                    <h4 id="parking-message">Parking</h4>
                                    <h4 id="electricity-message">Electricity</h4>
                                    <h4 id="well-message">Well</h4>
                                    <h4 id="tools-message">Tools</h4>
                                    <h4 id="warehouse-message">Warehouse</h4>
                                    <h4 id="farmyard-message">Farmyard</h4>
                                    <h4 id="greenhouse-message">Greenhouse</h4>
                                </div>
                                <div class="col-lg-3 help-ico"> 
                                </div>
                            </div>
                        </div> 
                    </div>

                </div>
               
                {{Form::button(Lang::get('publish_orchard.back_button'), array('class' => 'btn btn-lg back-button','id' => 'back-button'))}}

                {{Form::button(Lang::get('publish_orchard.next_button'), array('class' => 'btn btn-lg next-button third-step','id' => 'next-button'))}}

            </fieldset>

            <fieldset>
                
                <div class="row">

                    <div class="publish-form-container col-lg-6">

                        <div class="col-lg-9 col-lg-offset-2 form-title">
                            <h2>@lang('publish_orchard.photos_title')</h2> 
                        </div>

                        <div class="col-lg-10 col-lg-offset-2">

                            <div class="row photos-container">
                                <div class="col-lg-3">
                                    {{Form::hidden('photo_category', 'orchards')}}
                                    <label for="file-input" id="upload-image">
                                    </label>
                                    <input type="file" name="photo_orchard" id="file-input"  accept="image/jpg, image/gif, image/jpeg, image/png">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-6 help">
                        <div class="image-form">
                            <div id="upload-photo-ico"> 
                            </div>
                        </div>
                    </div>

                </div>

                {{Form::button(Lang::get('publish_orchard.back_button'), array('class' => 'btn btn-lg back-button','id' => 'back-button'))}}
        
                {{Form::button(Lang::get('publish_orchard.finish_button'), array('class' => 'btn btn-lg finish-button','id' => 'publish-orchard'))}}
                
            </fieldset>

            <div class="progress-bar progress-bar-striped active breadcrumbs" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
            

        {{Form::close()}}

        </section>

    </div>
</div>

@endsection

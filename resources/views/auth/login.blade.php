@extends('layouts.master')

@section("content")

<div class="container">
    <div class="row">

        <section class="new-orchard col-lg-12">
                    @if (Auth::guest())
                        <h2>no estás logueado</h2>
                    @else
                        <h2>estás logueado</h2>
                    @endif

        </section>

    </div>
</div>

@endsection

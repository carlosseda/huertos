<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="login">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="container">
				    <div class="row">
				        <div class="col-lg-12">

				        	<div class="row authenticate-error authenticate-error-login">
				        		<div class="col-lg-2 authenticate-error-ico">
									<img src="{{ URL::asset('images/ico/exclamation-ico.png') }}" alt="exclamation">
				        		</div>
				        		<div class="col-lg-10 authenticate-error-message">
				        			<p>Email o contraseña incorrecta</p>
				        		</div>
				        	</div>
				        
		                    <form class="form-horizontal" id="login-form" role="form">
		                        {{ csrf_field() }}

		                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"> 
		                            <div class="col-lg-12">
		                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo electrónico" required autofocus>

		                                @if ($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		                            <div class="col-lg-12">
		                                <input id="password" type="password" class="form-control" name="password" 
		                                placeholder="Contraseña" required>

		                                @if ($errors->has('password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="row remember-recover">
		                            <div class="col-lg-5">
		                                <div class="checkbox">
		                                    <label>
		                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuérdame
		                                    </label>
		                                </div>
		                            </div>

		                            <div class="col-lg-7" id="recover-password">
		                                <a href="{{ route('password.request') }}">
		                                    ¿Has olvidado tu contraseña?
		                                </a>
		                            </div>
		                        </div>

		                        <div class="form-group">
		                            <div class="col-lg-12">
		                                <button type="submit" id="login-button" class="btn btn-primary">
		                                    Iniciar sesión
		                                </button>
		                            </div>
		                        </div>
		                          
		                    </form>

	                    </div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contenido de los foormularios para la publicación de parcelas
    |--------------------------------------------------------------------------
    |
    | 
    |
    */

    'location_title' => '¿Dónde se encuentra tu parcela?',
    'province_option' => 'Elige tu provincia',
    'location_option' => 'Elige tu municipio',
    'description_title' => 'Vamos a preparar una presentación...',
    'ad_title' => 'Título del anuncio',
    'price_option' => 'Precio mensual (€)',
    'area_option' => 'Superficie (m²)',
    'description_option' => 'Describe la parcela... ',
    'services_title' => '¿Qué servicios ofreces?',
    'photos_title' => 'Añade algunas fotos...',
    'back_button' => '<<< Volver atrás',
    'next_button' => 'Siguiente paso >>>',
    'finish_button' => 'Finalizar'
];


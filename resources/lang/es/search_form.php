<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contenido del buscador de parcelas de la pantalla de inicio
    |--------------------------------------------------------------------------
    |
    | 
    |
    */

    'location_label' => '¿Dónde quieres cultivar?',
    'location_placeholder' => 'En cualquier lugar',
    'submit_button' => 'Buscar',
    
];


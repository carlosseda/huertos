<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contenido de la presentación individual de parcela
    |--------------------------------------------------------------------------
    |
    | 
    |
    */

    'title' => 'Alquiler de parcela en',
    'locate' => ', Mallorca',
    'price' => '€ / mes',
    'area' => 'm²',
    'form_title' => 'Habla con el anunciante',
    'message_field' => '¿Quieres cultivar en esta parcela? ¡Habla con el anunciante!',
    'description_title' => 'Descripción de la parcela',
    'services' => 'Servicios disponibles en la parcela',
    'email_label' => 'Tu email',
    'telephone_label' => 'Tu teléfono',
    'name_label' => 'Tu nombre',
    'submit_button' => 'Contactar',
    'disclaimer_message' => '<a href="">Política de Privacidad</a>',
    'parking_label' => 'Aparcamiento',
    'electricity_label' => 'Electricidad',
    'well_label' => 'Pozo',
    'warehouse_label' => 'Almacen',
    'tools_label' => 'Herramientas',
    'fenced_label' => 'Cercado',
    'farmyard_label' => 'Corral',
    'greenhouse_label' => 'Invernadero',
];


<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contenido del carrusel de parcelas destacadas
    |--------------------------------------------------------------------------
    |
    | 
    |
    */
    'title' => 'Parcelas destacadas',
    'orchard-title' => 'Parcela en',
    'locate' => ', Mallorca',
    'area' => 'Tamaño: ',
    'price' => 'Precio: ',
    'area_suffix' => 'm²',
    'price_suffix' => '€ / mes',
];



<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contenido de las dos ventanas "modal": "anuncia tu parcela" y "accede a 
    | tu cuenta". 
    |--------------------------------------------------------------------------
    |
    | 
    |
    */

    'title_form_add' => 'Anuncia tu parcela',
    'title_form_login' => 'Accede a tus anuncios',
    'photo_label' => 'Sube una foto...',
    'location_label' => 'Ubicación de la parcela',
    'area_label' => 'Superficie (m²)',
    'price_label' => 'Precio mensual (€)',
    'services_label' => 'Marca los servicios de los que dispone la parcela',
    'warehouse_button' => 'Almacém',
    'tools_button' => 'Herramientas',
    'parking_button' => 'Parking',
    'fenced_button' => 'Cercado',
    'description_field' => 'Describe la parcela que quieres alquilar... ',
    'new_user_label' => 'Soy un nuevo usuario',
    'registered_user_label' => 'Ya tengo una cuenta',
    'new_email_label' => 'Escribe tu email',
    'repeat_email_label' => 'Escribe de nuevo tu email',
    'new_password_label' => 'Elige una contraseña',
    'disclaimer_message' => 'Al registrarme acepto las Condiciones del servicio y la Política de Privacidad.',
    'email_login_label' => 'Email',
    'password_login_label' => 'Contraseña',
    'recover_password' => '¿Has olvidado tu contraseña?',
    'error_email_not_equal' => '*Los correos introducidos no coinciden.',
    'error_email_exist' => '*El correo introducido ya se encuentra registrado.',
    'error_password' => '*Email o contraseña incorrecta.',
    'submit_button' => 'Crear anuncio',

];


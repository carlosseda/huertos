<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contenido del pié de página
    |--------------------------------------------------------------------------
    |
    | 
    |
    */

    'title_filters' => 'Filtros de búsqueda',
    'title_results' => 'Parcelas para alquilar en',
    'optional_locate' => ', Mallorca',
    'orchard-title' => 'Parcela en',
    'price-filter-label' => 'Rango de precios:',
    'price-filter' => '€',
    'area-filter' => 'm²',
    'area-filter-label' => 'Tamaño de la parcela:',
    'services-filter-label' => 'Servicios disponibles',
    'electricity-filter-label' => 'Electricidad',
    'well-filter-label' => 'Pozo',
    'parking-filter-label' => 'Aparcamiento',
    'fenced-filter-label' => 'Cercado',
    'warehouse-filter-label' => 'Almacén',
    'tools-filter-label' => 'Herramientas',
    'farmyard-filter-label' => 'Corral',
    'greenhouse-filter-label' => 'Invernadero',
    'filter-results-button' => 'Filtrar resultados',
    'locate' => 'Mallorca',
    'area' => 'Tamaño: ',
    'price' => 'Precio: ',
    'area_suffix' => 'm²',
    'price_suffix' => '€ / mes'
    
];

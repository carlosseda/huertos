<?php

/*
    |--------------------------------------------------------------------------
    | Controlador de las funcionalidades CRUD de las parcelas
    |--------------------------------------------------------------------------
    | **addOrchard: Se recoge con $request los datos recopilados a través de formData con ajax en el apartado "Envío de anuncio"
    |   dentro del archivo main.js. Se recoge la ID del usuario que se encuentra logueado, y se crea la nueva ficha. El bucle for recorre 
    |   todas las fotografías que se han cargado, y la envía una a una para ser redimensionadas y guardadas.   
    |  
    | **displayOrchard: Se devuelve la ficha de una parcela concreta por su ID, se recoge la lista de pueblos y ciudades en
    |   locations porque se volverá a cargar modal_forms_add en la vista single_orchard.
    |
    | **displayOrchards: Devuelve todas las parcelas a views/search-result-container, siempre serán filtradas por provincia y
    |   opcionalmente por localidad. El precio/area mínimo y máximo son usados en views/search-result-filters para marcar los   
    |   límites de los sliders con los rangos de precio y tamaño. $Local recoge el nombre de la localidad para ser utilizada en los 
    |   titulos. 
    | 
    |    
    |    
    */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PhotosOrchardsController as Photo;
use App\location as Location;
use App\Orchard as Orchard;
use App\User as User;

class OrchardsController extends Controller
{
    public function publishOrchard(Request $request)
    {
        $data = $request->all();
        $data["users_ID"] = Auth::user()->ID;
        $orchard = Orchard::create($data);

        for ($i=0; $i < $request->photo_number; $i++) {
            $photo = "photo_" . $i; 
            Photo::insertPhoto($request->$photo, $request->photo_category, $orchard->id, $request->location_path);
        }

        echo json_encode($orchard->id);
    }

    public function displayOrchard($id)
    {
    	$orchard = Orchard::displayOrchard($id);
    	return View('single_orchard', compact('orchard'));
    }

    public function displayOrchards($province, $location = NULL)
    {
        $orchards = Orchard::displayOrchards($province, $location)->get();
        $min_price = Orchard::minPrice()->first()->price;
        $max_price = Orchard::maxPrice()->first()->price;
        $min_area = Orchard::minArea()->first()->area;
        $max_area = Orchard::maxArea()->first()->area;

        if($location){
            $local = Location::getLocal($location)->first()->local;
        }

        return View('search_result', compact('orchards', 'local', 'min_price','max_price','min_area','max_area'));
    }
}


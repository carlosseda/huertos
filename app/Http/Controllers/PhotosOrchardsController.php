<?php

/*
    |--------------------------------------------------------------------------
    | Controlador de las funcionalidades con las fotos de parcelas subidas 
    |--------------------------------------------------------------------------
    |
    | **insertPhoto: recoge los parametros que se encuentran en las tablas photo_settings y photo mediante la categoría que ha sido 
    |   recibida mediante un input-hidden nombrado photo_category en modal_forms_add. A continuación se hace uso del método
    |   extendido uploadPhoto que permite subir las fotos al servidor. Finalmente se guarda en la base de datos mediante foreach por 
    |   cada tamaño de foto. 
    |   
    |    
    */

namespace App\Http\Controllers;

use App\PhotoOrchard as Photo;

class PhotosOrchardsController extends PhotosController
{
    static public function insertPhoto($file, $category, $foreign_ID, $Opc_path=NULL){

        $photo["settings"] = Photo::getSettings($category);
        $photo["title"] = round(microtime(true));
        $photo["orchards_ID"] = $foreign_ID;

        self::uploadPhoto($file, $photo["title"], $photo["settings"], $Opc_path);

        foreach ($photo["settings"] as $setting => $value){
            $photo["photos_settings_ID"] = $value->ID;
            Photo::create($photo);
        }
    }
}

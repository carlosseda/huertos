<?php

/*
    |--------------------------------------------------------------------------
    | Controlador de las funcionalidades de búsqueda
    |--------------------------------------------------------------------------
    |    
    |   **autocompleteLocations: Hace una búsqueda de ubicaciones, opcionalmente se puede filtrar por provincia. Utilizado a través de search_form y 
    |   la librería easy-autocomplete en main.js
    |
    |   **getPath: Recoge la url-path de la localidad elegida en el buscador, a través de main.js autocompletado. Faltaría 
    |    hacer más flexible la búsqueda para que si la caja de búsqueda se enviara vacía devolviera resultados
    |    geolocalizados. [14-05-2017]
    |
    |   **filters: Hace uso de la macro if, creada en /providers/FiltersServiceProvider, para simplificar el código. Maneja
    |   los filtros en los resultados de búsqueda a través de main.js filtrar resultados. Faltaría conseguir que en la url 
    |   vayan apareciendo los filtros añadidos, a continuación se añaden pruebas realizadas con fullUrlWithQuery (laravel) y 
    |   http_build_query (nativo php) [13-05-2017]
    |
    |   $filters = $request->fullUrlWithQuery(['precio_entre' => $request->min_price, 'hasta' => $request->max_price . "metros_cuadrados"]);  
    |
    |   ¿array_merge?
    |
    |   $values["precio_desde"] = $request->min_price;
    |   $values["hasta"] = $request->max_price;
    |   $values = http_build_query($values);
    |   $replace = array("&", "=");
    |   $parameters = str_replace($replace,"_",$values);
    |
    |   return  $parameters;
    |
    |
    */

namespace App\Http\Controllers;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use App\Location as Location;
use App\Orchard as Orchard;

class SearchesController extends Controller
{
    public function autocompleteLocations($location, $opt_province = NULL)
    {
    	return Location::autocompleteLocations($location, $opt_province)->get();
    }

    public function getPath(Request $request)
    {
    	if ($request->locations == ""){
            $path["location"] = "";
    		$path["province"] = "mallorca";

    		echo json_encode($path);
    	}else{
    		$path = Location::getPath($request->locations)->first();

    		echo json_encode($path);
    	}
    }

    public function getLocation(Request $request)
    {
        $location = Location::getLocation($request->location)->first();

        echo json_encode($location);
    }

    public function filters(Request $request, $province, $location = NULL)
    {

        $orchards = Orchard::displayOrchards($province, $location)
                ->whereBetween('area', array($request->min_area, $request->max_area))
                ->whereBetween('price', array($request->min_price, $request->max_price))
                ->if($request->electricity, 'electricity', '=', $request->electricity)
                ->if($request->well, 'well', '=', $request->well)
                ->if($request->parking, 'parking', '=', $request->parking)
                ->if($request->fenced, 'fenced', '=', $request->fenced)
                ->if($request->warehouse, 'warehouse', '=', $request->warehouse)
                ->if($request->tools, 'tools', '=', $request->tools)
                ->if($request->farmyard, 'farmyard', '=', $request->farmyard)
                ->if($request->greenhouse, 'greenhouse', '=', $request->greenhouse)
                ->get();

        if ($location){

            $local = Location::getLocal($location)->first()->local;
        }
    
        return view('search_result_container', compact('orchards','local'));
    }

}


<?php

/*
    |--------------------------------------------------------------------------
    | Controlador de las funcionalidades con las fotos subidas 
    |--------------------------------------------------------------------------
    |
    | Se hace uso de la siguiente librería para el tratamiento de imágenes: http://image.intervention.io/
    |
    | **uploadPhoto: $file recoge la imagen que se ha introducido en input-file, $settings son recogidos de la tabla de la 
    |   base de datos photo_settings y photo donde se especifica la extensión que tendrá el archivo guardado, la calidad de la 
    |   imagen, su alto (no se especifica ancho porque se respetará las proporciones de la original), la carpeta donde será 
    |   guardada, y la personalización de la ruta url (path), $opc_path recoge la ubicación de la parcela. Con todo ello se 
    |   compone el nombre de las imágenes junto a su $title. Se realiza un foreach porque se guarda cada imagen en los distintos
    |   formatos de tamaño que requieren las diferentes partes de la web. 
    |   
    | **checkPhoto: Método antiguo de validar una foto antes de utilizar AddOrchardRequest o JqueryValidator. Editado 
    |   php.ini con los valores  definidos aquí para archivos grandes http://php.net/manual/es/features.file-upload.php#107406 
    |   
    |    
    */

namespace App\Http\Controllers;

use App\PhotoOrchard;
use Intervention\Image\Facades\Image;


class PhotosController extends Controller
{
    static public function uploadPhoto($file, $title, $settings, $Opc_path=NULL){

        foreach ($settings as $setting => $value) {

            Image::make($file)->encode($value->extension, $value->quality)->heighten($value->height)->save($value->directory .
                $title . $value->path . $Opc_path . '-' . $value->size . '.' . $value->extension);

        }
    }

    /*
    public function checkPhoto($photo,$category){


        $check = getimagesize($photo["tmp_name"]);
        $photo_file = $this->getDirectory($category) . basename($photo["name"]);
        $imageFileType = pathinfo($photo_file,PATHINFO_EXTENSION);

        if ($check !== false) {
            $uploadOk = 1;
        } else {
            $uploadOk = 0;
        }

        if ($photo["size"] > 8000000) {
            $uploadOk = 0;
        }

        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            $uploadOk = 0;
        }

        return $uploadOk;
    }*/
}

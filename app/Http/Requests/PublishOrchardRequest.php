<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/*
    |--------------------------------------------------------------------------
    | Controlador de las funcionalidades con las fotos de parcelas subidas 
    |--------------------------------------------------------------------------
    |
    | **rules: recoge las normas que se deben cumplir para validar el formulario. Los errores son devueltos en forma de objeto 
    |   JSON en un error 422.
    | 
    | **messages: mensajes personalizados de error. Por ahora sólo aparecen en la consola, no se han manipulado para aparecer en la 
    |   vista, jquery validator hace esa función y se detalla en el apartado "validaciones" en main.js
    |   
    |   
    |    
    */

class PublishOrchardRequest extends FormRequest
{
    /**
     * Determina si el usuario está validado para hace el request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Recoge las normas que hay que cumplir en el request.
     *
     * @return array
     */

    static public function rules()
    {

        return [
            
        ];
    }

    /**
     * Mensajes de error modificados, recogidos de la base de datos contents_forms_errors.
     *
     * @return array
     */

    public function messages()
    {

        return [


        ];
    }
}

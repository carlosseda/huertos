<?php

/*
    |--------------------------------------------------------------------------
    | Modelo para las localizaciones de las parcelas 
    |--------------------------------------------------------------------------
    |   **getProvinces: recoge la lista de pueblos y ciudades que será usada para rellenar los select de search_form y 
    |     modal_forms_add. oldest() equivale a order_by.
    |    
    */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Province extends Model
{

    //relaciones

    public function locations(){
        return $this->hasMany(Location::class);
    }

    //consultas

    static public function scopeGetProvinces($query)
    {
        $query->select('ID','province','path')->oldest('province');
    }
}

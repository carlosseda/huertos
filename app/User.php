<?php

/*
    |--------------------------------------------------------------------------
    | Modelo para los usuarios registrados
    |--------------------------------------------------------------------------
    |
    | !!protected $primaryKey, necesario para que se mantenga la sesión del login. Solución encontrada en: 
    |   https://stackoverflow.com/questions/21603347/laravel-authattempt-will-not-persist-login
    |
    | **scopeGetUser, scopeGetPassword, scopeGetID: utilizados para verificar la existencia del usuario a la hora de crear una nueva |   parcela. 
    |   
    |    
    */


namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    //propiedades

    protected $primaryKey = 'ID';

    protected $fillable = ['name', 'email', 'password',];

    protected $hidden = ['password', 'remember_token','created_at','updated_at', 'active'];

    //relaciones

    public function orchards(){
        return $this->hasMany(Orchard::class);
    }
}

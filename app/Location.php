<?php

/*
    |--------------------------------------------------------------------------
    | Modelo para las parcelas 
    |--------------------------------------------------------------------------
    |   **getLocations: recoge la lista de pueblos y ciudades que será usada para rellenar los select de search_form y 
    |     modal_forms_add. oldest() equivale a order_by.
    |
    |   **getPath: recoge el path-url de una localidad.
    |
    |   **getLocal: recoge el título de una localidad. 
    |
    |   **autocompleteLocations: Se ha añadido un índice "full text" a la columna 'local', match/against da una puntuación
    |   a cada búsqueda según la similitud con las palabras buscadas, siendo + * el indicador de que se valoren similitudes por
    |   delante y detrás de la palabra. Faltaría conseguir que una búsqueda de tipo 'pollensa' devolviera 'pollença' por similitud,
    |   se debe valorar la inserción de un motor de búsqueda tipo Lucene. A parte, se cuenta el número de parcelas que hay de cada
    |   localidad para mostrarlo en el desplegable. Opcionalmente permite filtrar por provincia a través de la ID de la provincia. [13-05-2017]
    |    
    */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Location extends Model
{

    //relaciones

    public function orchard(){
        return $this->hasMany(Orchard::class);
    }

    //consultas

    static public function scopeGetLocations($query)
    {
        $query->select('ID','local','path')->oldest('local');
    }

    static public function scopeGetLocation($query, $location)
    {
        $query->leftJoin('orchards', 'locations.ID', '=', 'orchards.locations_ID')
        ->where('local', $location)
        ->groupBy('locations.ID')
        ->select('locations.ID','local','path', DB::raw('AVG(orchards.price) as price'), DB::raw('AVG(orchards.area) as area'));
    }

    static public function scopeGetPath($query, $location)
    {
        $query->select('locations.path as location','provinces.path as province')
        ->leftJoin('provinces', 'locations.provinces_ID', '=', 'provinces.ID')
        ->where('local', $location);
    }

    static public function scopeGetID($query, $location)
    {
        $query->select('locations.ID')
        ->where('local', $location);
    }

    static public function scopeGetLocal($query, $location)
    {
        $query->select('local')->where('path', $location);
    }


    static public function scopeAutocompleteLocations($query, $search, $opt_province = NULL)
    {

        $query->select('local', DB::raw("match(local) against ('+{$search}*' in boolean mode) as revelance"), DB::raw("count(orchards.id) as total"))
            ->leftJoin('orchards', 'locations_ID', '=', 'locations.ID')
            ->whereRaw("match(local) against ('+{$search}*' in boolean mode)")
            ->if($opt_province, 'provinces_ID', '=', $opt_province)
            ->groupBy('local');
            
            /*SELECT local, count(orchards.id) as total FROM locations LEFT JOIN orchards ON orchards.locations_ID = locations.ID WHERE local LIKE '%%' GROUP BY local*/

            /*otra alternativa de búsqueda*/

            /*$query->select('local', DB::raw("count(orchards.id) as total"))
            ->leftJoin('orchards', 'orchards.locations_ID', '=', 'locations.ID')
            ->whereRaw('local', 'like', $search)
            ->groupBy('local','locations.id'); */
    }
}

<?php

/*
    |--------------------------------------------------------------------------
    | Modelo para las parcelas 
    |--------------------------------------------------------------------------
    |   En las relaciones se especifica que una parcela puede tener muchas fotografías, se prepara el terreno para en un futuro 
    |   permitir subir más de una fotografía [01/05/2017]
    |
    |   **displayOrchard: carga los datos de la ficha de una parcela.
    |
    |   **displayOrchards: carga los datos de todas las parcelas, pudiendo ser filtradas por provincia y localidad. 
    |
    |    
    */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Orchard extends Model
{

    //propiedades

    protected $fillable = ['locations_ID','users_ID','area', 'price', 'tools', 'warehouse', 'parking', 'fenced', 
    'electricity', 'well','farmyard', 'greenhouse','ad_title','description'];
    
    protected $hidden = ['created_at','update_at','active'];

    //relaciones

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function location(){
        return $this->belongsTo(Location::class);
    }

    public function photos(){
        return $this->hasMany(PhotoOrchard::class);
    }

    //consultas

    static public function displayOrchard($id)
    {
        $orchard = DB::table('orchards')
            ->select('locations.local','locations.path as location_path','area','price', 'tools', 'warehouse', 'parking', 'fenced', 'electricity', 'well', 'farmyard', 'greenhouse','description', 'title as photo_title','directory','photos.path as photo_path','size','extension')
            ->join('locations','orchards.locations_ID','=','locations.ID')
            ->join('photos_orchards', 'orchards.ID','=','photos_orchards.orchards_ID')
            ->join('photos_settings','photos_orchards.photos_settings_ID','=','photos_settings.photos_ID')
            ->join('photos', 'photos_settings.photos_ID','=','photos.ID')
            ->where('orchards.ID', $id)
            ->where('size', 'large')
            ->first();

         return $orchard;
    }

    static public function displayOrchards($opt_province = NULL, $opt_location = NULL)
    {
        $orchards = DB::table('orchards')
            ->select('locations.local','locations.path as location_path','orchards.id as id','area','price', 'photos_orchards.title as photo_title','directory','photos.path as photo_path','size','extension')
            ->join('locations','orchards.locations_ID','=','locations.ID')
            ->join('provinces','locations.provinces_ID','=','provinces.ID')
            ->join('photos_orchards', 'orchards.ID','=','photos_orchards.orchards_ID')
            ->join('photos_settings','photos_orchards.photos_settings_ID','=','photos_settings.photos_ID')
            ->join('photos', 'photos_settings.photos_ID','=','photos.ID')
            ->if($opt_province, 'provinces.path', '=', $opt_province)
            ->if($opt_location, 'locations.path', '=', $opt_location)
            ->where('size', 'medium');

        return $orchards;

        /*SELECT locations.local, locations.path as location_path, orchards.id as id, area, price, title as photo_title, directory, photos.path as photo_path, size, extension FROM orchards INNER JOIN locations ON orchards.locations_ID = locations.ID INNER JOIN photos_orchards ON orchards.ID = photos_orchards.orchards_ID INNER JOIN photos_settings ON photos_orchards.photos_settings_ID = photos_settings.photos_ID INNER JOIN photos ON photos_settings.photos_ID = photos.ID WHERE size = 'medium' */
    }

    static public function minPrice($opt_location = NULL)
    {
        $min_price = DB::table('orchards')
            ->select('price')
            ->join('locations','orchards.locations_ID','=','locations.ID')
            ->if($opt_location, 'locations.path', '=', $opt_location)
            ->orderBy('price', 'ASC');

        return $min_price;
    }

    static public function maxPrice($opt_location = NULL)
    {
        $max_price = DB::table('orchards')
            ->select('price')
            ->join('locations','orchards.locations_ID','=','locations.ID')
            ->if($opt_location, 'locations.path', '=', $opt_location)
            ->orderBy('price', 'DESC');

        return $max_price;
    }

    static public function minArea($opt_location = NULL)
    {
        $min_area = DB::table('orchards')
            ->select('area')
            ->join('locations','orchards.locations_ID','=','locations.ID')
            ->if($opt_location, 'locations.path', '=', $opt_location)
            ->orderBy('area', 'ASC');

        return $min_area;
    }

    static public function maxArea($opt_location = NULL)
    {
        $max_area = DB::table('orchards')
            ->select('area')
            ->join('locations','orchards.locations_ID','=','locations.ID')
            ->if($opt_location, 'locations.path', '=', $opt_location)
            ->orderBy('area', 'DESC');

        return $max_area;
    }

}

<?php

/*
    |--------------------------------------------------------------------------
    | Modelo para las fotografías
    |--------------------------------------------------------------------------
    |	Modelo usado para recoger las características que deben tener las fotografías que se subirán al servidor. En cada formulario
    |	junto al input-file habrá un input-hidden que especificará la categoría de la fotografía que se intenta subir. 
    |    
    */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Photo extends Model
{

    //consultas
    
   static public function getSettings($category){

        $settings = DB::table('photos')
            ->select('photos_settings.ID','directory','path','size','height','extension','quality')
            ->join('photos_settings','photos.ID','=','photos_settings.photos_ID')
            ->where('category',$category)
            ->get();

        return $settings;
    }
}


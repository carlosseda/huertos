<?php

/*
    |--------------------------------------------------------------------------
    | Modelo para las fotografías de parcelas
    |--------------------------------------------------------------------------
    |	
    |    
    */

namespace App;
use Illuminate\Database\Eloquent\Model;

class PhotoOrchard extends Photo
{
    //propiedades
    
    protected $table = 'photos_orchards';
    protected $fillable = ['title','photos_settings_ID','orchards_ID'];

    //relaciones

    public function orchard(){
        return $this->belongsTo(Orchard::class);
    }
}


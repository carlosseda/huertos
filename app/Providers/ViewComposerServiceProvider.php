<?php

/*
    |--------------------------------------------------------------------------
    | Provider para contenidos recurrentes de a web
    |--------------------------------------------------------------------------
    |   Se sigue la estructura de este ejemplo: https://laracasts.com/series/laravel-5-fundamentals/episodes/25 
    |   modificado el archivo config/app providers para incluir este archivo. 
    |
    |   Con este archivo evitamos tener que repetir código solicitando los contenidos que se repiten en cada vista. 
    |
    |   **composeProvinces: listado de las provincias para el select del formulario en publish_orchard_location
    |
    |   **composeFeatured: listado de las parcelas destacadas para ser visualizadas en la Home. No filtra por provincia, por ahora 
    |     muestra todas las destacadas de cualquier provincia. Se buscará geolocalizar los resultados. [14-05-2017] 
    |
    |    
    */


namespace App\Providers;

use App\Province;
use App\Orchard;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeProvinces();
        $this->composeFeatured();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function composeProvinces()
    {
        view()->composer('publish_orchard', function ($view)
        {
            $view->with('provinces', Province::getProvinces()->get());
        });
    }

    private function composeFeatured()
    {
        view()->composer('featured', function ($view)
        {
            $view->with('featured', Orchard::displayOrchards()->where('featured', 'y')->get());
        });
    }
}

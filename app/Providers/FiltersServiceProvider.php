<?php

/*
    |--------------------------------------------------------------------------
    | Provider para diseño de consultas
    |--------------------------------------------------------------------------
    |   Se sigue la estructura de este ejemplo: 
    |   https://github.com/kevinsimard/laravel-builder-macros/blob/master/src/Providers/OrderByRandomServiceProvider.php
    |
    |   Con este archivo confeccionamos macros para las búsquedas realizadas con Query Builder:
    |
    |   *if: Uso del condicional, si un valor existe en el request entonces se realiza la búsqueda en la consulta. Método utilizado 
    |   para filtrar a la hora de buscar parcelas. 
    |
    |    
    */


namespace App\Providers;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\ServiceProvider;

class FiltersServiceProvider extends ServiceProvider

{
    public function register()
    { 
        Builder::macro('if', function ($condition, $column, $operator, $value) {
            if ($condition) {
                return $this->where($column, $operator, $value);
            }

            return $this;
        });
    }
}
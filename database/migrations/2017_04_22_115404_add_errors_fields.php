<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddErrorsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents_forms_errors', function (Blueprint $table) {

            $table->string('area_error',300);
            $table->string('price_error',300);
            $table->string('warehouse_error',300);
            $table->string('tools_button_error',300);
            $table->string('parking_error',300);
            $table->string('fenced_error',300);
            $table->string('description_error',300);
            $table->string('email_error',300);
            $table->string('password_error',300);
            $table->string('registered_email_error',300);
            $table->string('registered_password_error',300);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents_forms_errors', function (Blueprint $table) {

            $table->dropColumn([
                'area_error', 'price_error', 'warehouse_error','tools_button_error', 'parking_error',
                'fenced_error','description_error', 'email_error','password_error', 'registered_email_error',
                'registered_password_error'
            ]);
        });
    }
}
